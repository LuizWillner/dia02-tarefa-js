// FizzBuzz

// Teste 5 números inteiros aleatórios. Os testes: 
// - Caso o valor seja divisível por 3, imprima no console “fizz”. 
// - Caso o valor seja divisível por 5 imprima “buzz”. 
// - Caso o valor seja divisível por 3 e 5, ao mesmo tempo, imprima “fizzbuzz”.
// - Caso contrário imprima nada.

let lista = [null, null, null, null, null]

for(num of lista) {
    num = Math.round(Math.random() * 100)

    if(num % 3 == 0 && num % 5 == 0) {
        console.log("fizzbuzz")
    }

    else if(num % 3 == 0) {
        console.log("fizz")
    }

    else if(num % 5 == 0) {
        console.log("buzz")
    }
}
