// Fahrenheit

// Escreva um algoritmo para ler uma temperatura em graus Fahrenheit, calcular
// e escrever o valor correspondente em graus Celsius (baseado na fórmula abaixo):
// C/5 =( f-32)/9

let tempFahr = Math.round(Math.random() * 100);

let tempCelcius = 5*((tempFahr - 32)/9);

console.log(tempFahr.toFixed(2) + " ºF = " + tempCelcius.toFixed(2) + " ºC");