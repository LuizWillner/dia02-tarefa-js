// Aluno

// Faça um algoritmo em que você recebe 3 notas de um aluno e caso a média aritmética dessas
// notas for maior que 6 imprima “Aprovado” e “Reprovado”.

let aluno = {
    nota1: null,
    nota2: null,
    nota3: null,
}

aluno.nota1 = Math.random() * 10
aluno.nota2 = Math.random() * 10
aluno.nota3 = Math.random() * 10
media = (aluno.nota1 + aluno.nota2 + aluno.nota3)/3


console.log("Media = " + media.toFixed(2));
console.log(media >= 6 ? "Aprovado":"Reprovado")